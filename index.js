const readlineSync = require('readline-sync');

console.log('=================================');
console.log('Challenge Chapter 2');
console.log('=================================');
console.log('Masukkan nilai siswa\n(masukkan "q" jika sudah selesai)');
console.log('=================================');

let data = readlineSync.question('Nilai : ');
let arrData = []

while(data !== "q"){
    if(data >= 0 && data <= 100) arrData.push(data);
    else console.log('invalid data');
    data = readlineSync.question('Nilai : ');
}

class ProsesData{
    constructor(data){
        this.data = data
    }
    hitungMean(){
        if(this.data.length === 0) return 0
        let sum = 0
        for(let nilai of this.data) sum += Number(nilai);
        let mean = sum / (this.data.length)
        return mean
    }
    hitungLulus(){
        let jumlahLulus = 0
        let jumlahTidakLulus = 0
        for (let nilai of this.data) Number(nilai) >= 75 ? jumlahLulus++ : jumlahTidakLulus++
        return {jumlahLulus,jumlahTidakLulus}
    }
    scoreMin(){
        if(this.data.length === 0) return 0
        let min = 101
        for (let nilai of this.data) if(Number(nilai) < min) min = Number(nilai);
        return min
    }
    scoreMax(){
        if(this.data.length === 0) return 0
        let max = 0
        for (let nilai of this.data) if(Number(nilai) > max) max = Number(nilai)
        return max
    }
}

console.log('=================================');
if(arrData.length > 0){
    const NilaiSiswa = new ProsesData(arrData)
    console.log(`Rata Rata Nilai\t\t : ${NilaiSiswa.hitungMean()}`)
    console.log(`Jumlah Siswa Lulus\t : ${NilaiSiswa.hitungLulus().jumlahLulus}`)
    console.log(`Jumlah Siswa Tidak Lulus : ${NilaiSiswa.hitungLulus().jumlahTidakLulus}`)
    console.log(`Score Terendah\t\t : ${NilaiSiswa.scoreMin()}`)
    console.log(`Score Tertinggi\t\t : ${NilaiSiswa.scoreMax()}`)
}else{
    console.log('Data Kosong');
}
console.log('=================================');